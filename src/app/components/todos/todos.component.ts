import { Component, OnInit } from '@angular/core';
import { Todo } from '../../models/Todo';
import { TodoService } from '../../Services/todo.service';
@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.sass']
})
export class TodosComponent implements OnInit {

  todos: Todo[];
  title: string = "Todo List";
  tasklabel: string = "Tâches du jour";
  constructor(private _todo: TodoService) { }

  ngOnInit(): void {
    this._todo.getTodos().subscribe(todos => {
      this.todos = todos;
    }); 
  }

  deleteTodo(todo: Todo) {
    //remove from UI
    this.todos = this.todos.filter(t => t.id !== todo.id);
    //removefrom server
    this._todo.deleteTodo(todo).subscribe();
  }

  addTodo(todo: Todo){
    this._todo.addTodo(todo).subscribe(t => {
      this.todos.push(t);
    });
  }

}
