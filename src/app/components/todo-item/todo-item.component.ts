import { Component, OnInit, Input, EventEmitter, Output  } from '@angular/core';
import { Todo } from 'src/app/models/Todo';
import { TodoService } from 'src/app/Services/todo.service';



@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.sass']
})
export class TodoItemComponent implements OnInit {

  @Input() todo: Todo;
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();

  constructor(private _todo: TodoService) { }

  ngOnInit(): void {
  }

  //Set dynamic classes
  setClasses() {
    let classes = {
      todo: true,
      'is-completed': this.todo.completed,

    }
    return classes;
  }

  onToggle(todo) {
    console.log('toggled');
    //toggle on UI
    this.todo.completed = !this.todo.completed;
    //toggle on server
    this._todo.toggleCompleted(todo).subscribe(todo => {
      console.log(todo);
    })
  }

  onDelete(todo) {
    console.log('deleted');
    this.deleteTodo.emit(todo);
  }

}
