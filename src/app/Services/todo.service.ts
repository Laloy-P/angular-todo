import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Todo } from '../models/Todo'
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  todosUrls: string = 'https://jsonplaceholder.typicode.com/todos';
  todosLimits = '?_limit=4';

  constructor(private _http: HttpClient) { }

  getTodos(): Observable<Todo []> {
    return this._http.get<Todo []>(`${this.todosUrls}${this.todosLimits}`);
  }

  //toogle completed
  toggleCompleted(todo: Todo):Observable<any> {
    const url = `${this.todosUrls}/${todo.id}`;
    return this._http.put(url, todo, httpOptions);
  }

  //delete todo
  deleteTodo(todo: Todo){
    const url = `${this.todosUrls}/${todo.id}`;
    return this._http.delete(url, httpOptions);
  }

  addTodo(todo: Todo):Observable<Todo>{
    return this._http.post<Todo>(this.todosUrls, todo, httpOptions);
  }

}
